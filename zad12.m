clc
clear

r =2 %mm
r1 = 2*25.4 %mm
m= 0.010 %kg
g=9.81 %m/s^2
t= 2 %s
G = 80000000000
hold on
for N=2:50;
    
T = 1
f0=m*g
k= (G*(r^4))/(4*N*(r1^3))
beta = 1/(2*t)
omega0 = sqrt(k/m)

alfa0=f0/m

for i=1:1000;
    omega =i/100;
    f=omega/(2*pi)
    y(i)=(alfa0/(sqrt((((omega0^2)-(omega^2))^2)+4*(beta^2)*(omega^2))))*sin((omega*t)+atan((2*beta*omega)/((omega0^2)-(omega^2))));
    x(i)=f;
    
end
plot(x,y)
end
hold off
